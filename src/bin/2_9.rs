#[path = "../crypto.rs"]
mod crypto;
#[path = "../buffer.rs"]
mod buffer;

fn main() {
    println!("-- Set 2, Challenge 9 --");

    println!("{}", buffer::stringify(&crypto::pkcs7_padding("YELLOW SUBMARINE", 20)));
}
