extern crate regex;
extern crate rustc_serialize;

use std::str;
use std::io::prelude::*;
use std::fs::File;
use rustc_serialize::base64::FromBase64;
use std::string::String;

#[path = "../buffer.rs"]
mod buffer;

#[path = "../profile.rs"]
mod profile;

fn test_key_lengths(cipherdata: &Vec<u8>) {
    // Key-length profiling
    for possible_key_length in 2..40 {
        let mut average = 0.0;
        let average_bins = 3;
        let mut block1 = cipherdata.clone();
        for _ in 0..average_bins {
            let mut block2 = block1.split_off(possible_key_length);
            average = average + profile::edit_distance(&block1, &block2) as f64 / average_bins as f64;
            block1 = block2.split_off(possible_key_length);
        }

        let normalized_distance = average / possible_key_length as f64;
        println!("Distance for {}-char key: {}", possible_key_length, normalized_distance);
    }
}

fn print_best_options(cipherdata: &Vec<u8>, key_length: usize) {
    let interleaved = buffer::interleave(&cipherdata, key_length);
    let english_frequencies = profile::english_frequencies();

    for (position, char_group) in interleaved.iter().enumerate() {
        // println!("Cracking position {}/{}\n{:?}\n\n", position + 1, key_length, char_group);
        println!("Cracking position {}/{}\n\n", position + 1, key_length);

        for i in 0..255 {
            let result = buffer::xor_vec(&char_group, vec![i]);

            let text = buffer::stringify(&result);
            let score = profile::score_text(text, &english_frequencies);
            if score < 0.4 {
                // println!("score: {}, group {}, key {}\n{}\n\n", score, position, i, text);
                println!("score: {}, group {}, key {}", score, position, i);
            }

            // // Preview high scorers
            // if (score < 0.2) {
            //     println!("score: {}, group {}, key {}\n{}\n\n", score, position, i, text);
            // }
        }
    }
}

fn main() {
    let test1 = "this is a test".to_string().into_bytes();
    let test2 = "wokka wokka!!!".to_string().into_bytes();
    assert_eq!(profile::edit_distance(&test1, &test2), 37);
    // println!("{}", profile::edit_distance(&test1, &test2));

    println!("-- Set 1, Challenge 6 --");

    let mut f = File::open("6.txt").unwrap();
    let mut ciphertext = String::new();
    f.read_to_string(&mut ciphertext).unwrap();

    let cipherdata = ciphertext.from_base64().unwrap();

    // test_key_lengths(&cipherdata);
    // return;

    // 5-character key looks best, also 2, 11, 13
    // 2-bin: 3, 13, 11
    // 3-bin 2, 5, 3, 13
    // 8-bin: 29, 5, 15
    let key_length = 29;

    // print_best_options(&cipherdata, key_length);

    let first_key_guess: Vec<u8> = vec![116, 101, 114, 109, 105, 110, 97, 116, 111, 114, 32, 120, 58, 32, 98, 114, 105, 110, 103, 32, 116, 104, 101, 32, 110, 111, 105, 115, 101];
    let refined_key: Vec<u8> =     vec![84, 101, 114, 109, 105, 110, 97, 116, 111, 114, 32, 88, 58, 32, 66, 114, 105, 110, 103, 32, 116, 104, 101, 32, 110, 111, 105, 115, 101];


    println!("key guess: \"{}\"\n", buffer::stringify(&refined_key));
    let decrypted = buffer::xor_vec(&cipherdata, refined_key);
    println!("decrypted:\n{}", buffer::stringify(&decrypted));
}

// Key "Terminator X: Bring the noise"
// Message is Vanilla Ice - Play That Funky Music
