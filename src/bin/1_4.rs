extern crate regex;
extern crate rustc_serialize;

use std::io::prelude::*;
use std::fs::File;
use std::io::BufReader;
use std::str;
use regex::Regex;
use rustc_serialize::hex::FromHex;

#[path = "../buffer.rs"]
mod buffer;

fn main() {
    println!("-- Set 1, Challenge 4 --");

    let f = File::open("4.txt").unwrap();
    let file = BufReader::new(&f);

    for (index, possible_cipher) in file.lines().enumerate() {
        let cipher = possible_cipher.unwrap();
        let possible_cipherdata = cipher.from_hex().unwrap();

        println!("Attempting to crack cipher #{}", index);

        for i in 0..254 {
            let result = buffer::xor_vec(&possible_cipherdata, vec![i]);
            let re = Regex::new(r"^[a-zA-Z\s',:;?!\(\)$]+$").unwrap();
            if re.is_match(buffer::stringify(&result)) {
                println!("Potential match with key: {} in ciphertext: {}", i, cipher);
                println!("{}", buffer::stringify(&result));
            }
        }
    }
}

// Potential match with key: 53 in ciphertext: 7b5a4215415d544115415d5015455447414c155c46155f4058455c5b523f
// Now that the party is jumping
