extern crate rustc_serialize;

use rustc_serialize::hex::{FromHex, ToHex};

fn main() {
    let input1 = "1c0111001f010100061a024b53535009181c";
    let input2 = "686974207468652062756c6c277320657965";

    let data1 = input1.from_hex().unwrap();
    let data2 = input2.from_hex().unwrap();

    let mut result = Vec::new();

    // Could possibly do .zip().map()
    for (index, value1) in data1.iter().enumerate() {
        let value2 = data2[index];
        result.push(value1 ^ value2);
    }

    let hex = result.to_hex();

    println!("-- Set 1, Challenge 2 --");
    println!("{}", hex);
}


// http://stackoverflow.com/questions/26185485/how-to-convert-hexadecimal-values-to-base64-in-rustlang
