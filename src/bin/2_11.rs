extern crate openssl;
extern crate rand;
extern crate rustc_serialize;

use std::io::prelude::*;
use std::fs::File;
use rustc_serialize::base64::FromBase64;
use openssl::symm::{decrypt, encrypt, Cipher};
use rand::Rng;
use rustc_serialize::hex::ToHex;


#[path = "../crypto.rs"]
mod crypto;
#[path = "../buffer.rs"]
mod buffer;
#[path = "../profile.rs"]
mod profile;

fn main() {
    println!("-- Set 2, Challenge 11 --");

    let random_key = crypto::rand_aes_key();

    println!("random aes key' {:?}", random_key);
    println!("random encryption of 'cool story bro' {:?}", rand_crypt("cool story bro".to_string().into_bytes()));
    println!("randonly padded YELLOW SUMBARINE {:?}", rand_byte_padding("YELLOW SUBMARINE".to_string().into_bytes()));

    // let input = "But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure? But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?";
    // let input = "123456788765432112345678876543211234567887654321";

    // Input must be 3 block lengths, allowing a 2-block repeat, even with random padding
    let input = "123456788765432112345678876543211234567887654321";
    let encrypted = black_box(&input);
    println!("encrypted text' {:?}", encrypted);

    profile::detect_ecb(encrypted.to_hex(), 16);
}

fn black_box(input: &str) -> Vec<u8> {
    let input_bytes = input.to_string().into_bytes();
    rand_crypt(rand_byte_padding(input_bytes))
}

fn rand_crypt(input_bytes: Vec<u8>) -> Vec<u8> {
    let key = crypto::rand_aes_key();

    match rand::thread_rng().gen_range(0, 2) {
        0 => {
            println!("CBC");
            crypto::cbc_encrypt(&input_bytes, &key, &crypto::rand_aes_key(), 16)
        },
        _ => {
            println!("ECB");
            crypto::ecb_encrypt(&key, &input_bytes)
        }
    }
}

fn rand_byte_padding(input: Vec<u8>) -> Vec<u8> {
    let prefix_len = rand::thread_rng().gen_range(5, 11);
    let suffix_len = rand::thread_rng().gen_range(5, 11);

    println!("padding with {} / {} bytes", prefix_len, suffix_len);

    let suffix: Vec<_> = rand::thread_rng()
        .gen_iter::<u8>()
        .take(suffix_len)
        .collect();

    // Mutable output vec: start with prefix bytes
    let mut output: Vec<_> = rand::thread_rng()
        .gen_iter::<u8>()
        .take(prefix_len)
        .collect();

    output.extend(input);
    output.extend(suffix);
    output
}
