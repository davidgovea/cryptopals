#![allow(dead_code)]
extern crate openssl;
extern crate rand;
extern crate rustc_serialize;

use std::str;
// use std::io::prelude::*;
// use std::fs::File;
// use rustc_serialize::base64::FromBase64;
use self::openssl::symm::{decrypt, encrypt, Cipher, Mode, Crypter};
use self::rand::{Rng};

#[path = "./buffer.rs"]
mod buffer;

pub fn ecb_encrypt(key: &Vec<u8>, data: &Vec<u8>) -> Vec<u8> {
    encrypt(Cipher::aes_128_ecb(), key.as_slice(), Some(&[0; 16]), data.as_slice()).unwrap()
}

pub fn ecb_decrypt(key: &Vec<u8>, cipherdata: &Vec<u8>) -> Vec<u8> {
    decrypt(Cipher::aes_128_ecb(), key.as_slice(), Some(&[0; 16]), cipherdata.as_slice()).unwrap()
}

pub fn ecb_decrypt_block(key: &Vec<u8>, cipherdata: &Vec<u8>) -> Vec<u8> {
    ecb_process_block(key, cipherdata, Mode::Decrypt)
}
pub fn ecb_encrypt_block(key: &Vec<u8>, plaintext: &Vec<u8>) -> Vec<u8> {
    ecb_process_block(key, plaintext, Mode::Encrypt)
}

pub fn ecb_process_block(key: &Vec<u8>, input: &Vec<u8>, mode: Mode) -> Vec<u8> {
    let mut crypter = Crypter::new(
        Cipher::aes_128_cbc(),
        mode,
        key,
        Some(&[0; 16])).unwrap();

    crypter.pad(false);

    let data_len = input.len();
    let block_size = Cipher::aes_128_cbc().block_size();
    let mut plaintext = vec![0; block_size + data_len];

    let mut count = crypter.update(input, &mut plaintext).unwrap();
    count += crypter.finalize(&mut plaintext[count..]).unwrap();
    plaintext[0..count].to_vec()
}

pub fn rand_aes_key() -> Vec<u8> {
    rand::thread_rng()
        .gen_iter::<u8>()
        .take(16)
        .collect()
}

pub fn pkcs7_padding(text: &str, length: usize) -> Vec<u8> {
    let text_length = text.len();
    let pad_count = length - text_length;
    let mut b = Vec::from(text);
    for _ in 0..pad_count {
        b.push(pad_count as u8);
    }

    b
}

pub fn cbc_decrypt(text: &Vec<u8>, key: &Vec<u8>, iv: &Vec<u8>, block_size: usize) -> Vec<u8> {
    cbc_crypter(text, key, iv, block_size, false)
}

pub fn cbc_encrypt(text: &Vec<u8>, key: &Vec<u8>, iv: &Vec<u8>, block_size: usize) -> Vec<u8> {
    cbc_crypter(text, key, iv, block_size, true)
}

fn cbc_crypter(input: &Vec<u8>, key: &Vec<u8>, iv: &Vec<u8>, block_size: usize, encrypt_mode: bool) -> Vec<u8> {
    let mut output = Vec::new();
    let mut xor_block = iv.to_vec();

    let mut to_process = input.len() as usize;
    let mut block_num = 0;
    while to_process >= block_size {
        let offset = block_size * block_num;
        let end_offset = offset + block_size;
        // Grab this block of ciphertext
        let input_block = &input[offset..end_offset].to_vec();

        let output_block: Vec<u8>;
        if encrypt_mode == true {
            // XOR with IV or previous ciphertext block, then encrypt
            output_block = ecb_encrypt_block(&key, &buffer::xor_vec(&input_block, xor_block));
            xor_block = output_block.to_vec();
        } else {
            // Decrypt, then XOR with IV or previous ciphertext block
            output_block = buffer::xor_vec(&ecb_decrypt_block(&key, &input_block), xor_block);
            xor_block = input_block.to_vec();
        }

        output.extend_from_slice(&output_block[..]);

        let rest = &input[end_offset..];
        to_process = rest.len();
        block_num = block_num + 1;
    }
    output
}
